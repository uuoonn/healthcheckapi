package com.lyj.healthcheckapi.controller;

import com.lyj.healthcheckapi.Service.HealthService;
import com.lyj.healthcheckapi.model.HealthRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/health")
public class HealthController {
    private final HealthService healthService;

    @PostMapping ("/people")
    public String setHealth(@RequestBody HealthRequest request){
        healthService.setHealth(request);

        return "OK";
    }
}

package com.lyj.healthcheckapi.Service;

import com.lyj.healthcheckapi.entity.Health;
import com.lyj.healthcheckapi.model.HealthRequest;
import com.lyj.healthcheckapi.repository.HealthRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class HealthService {
    private final HealthRepository healthRepository;

    public void setHealth (HealthRequest request){
        Health addDate = new Health();
        addDate.setDateCreate(LocalDate.now());
        addDate.setName(request.getName());
        addDate.setPeopleStatus(request.getPeopleStatus());
        addDate.setEtcMemo(request.getEtcMemo());

        healthRepository.save(addDate);
    }


}

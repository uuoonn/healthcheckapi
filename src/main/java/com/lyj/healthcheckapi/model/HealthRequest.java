package com.lyj.healthcheckapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
//사람이 적어야하는 부분만 entity에서 복사해오기.
//id나 날짜의 경우는 자동이기 때문에 모델에 적지않는다.
public class HealthRequest {
    private String name;
    private String peopleStatus;
    private String etcMemo;
}
